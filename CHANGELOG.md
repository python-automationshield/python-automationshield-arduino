# Changelog

<!--next-version-placeholder-->

## v1.0.0 (03/06/2024)

- Separate `python-automationshield-arduino` from `python-automationshield` repository.
