# python-automationshield-arduino

`python-automationshield-arduino` contains the Arduino code required for [`python-automationshield`](https://gitlab.com/mrtreasurer/python-automationshield). This repository currently contains implementations for the following boards and [AutomationShield](https://github.com/gergelytakacs/AutomationShield) devices:

| Board    | AeroShield | FloatShield | MagnetoShield |
|----------|------------|-------------|---------------|
| Leonardo | ✅          | ✅           | ✅             |
| Uno      | ✅¹         | ✅¹          | ✅¹            |
| Mega (2560)     | ✅¹         | ✅¹          | ✅¹            |

* ¹: Implementation not tested on hardware (due to lack thereof)

## Contributing

Interested in contributing? Check out the contributing guidelines. Please note that this project is released with a Code of Conduct. By contributing to this project, you agree to abide by its terms.

## License

`python-automationshield-arduino` was created by Bert Van den Abbeele. It is licensed under the terms of the MIT license.
