#include <floatshield.h>

FloatShield shield;

void setup() {
  shield.setup();
}

void loop() {
  shield.loop();
}