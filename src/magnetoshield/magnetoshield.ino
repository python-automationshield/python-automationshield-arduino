#include <magnetoshield.h>

MagnetoShield shield;

void setup() {
  shield.setup();
}

void loop() {
  shield.loop();
}